anime({
    targets: '#test-1',
    translateX: 100,
    scale: [.5, 1],
    rotate: 180,
    delay: 1000,
    opacity: [0, 1],
    duration: 2000,
    easing: 'easeOutExpo',
    complete: function() {
        anime({
            targets: '#test-1',
            translateX: 200,
            scale: [1, 2],
            rotate: 360,
            delay: 5000,
            opacity: 0,
            duration: 1000,
            easing: 'easeOutExpo'
        });
    }
});
